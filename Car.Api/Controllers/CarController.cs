﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Car.Api.Controllers
{
    [Route("api/car")]
    [ApiController]
    public class CarController : ControllerBase
    {

        [HttpGet, Route("")]
        public IActionResult GetCars(string search)
        {
            var result = new List<Cars>
            {
                new Cars
                {
                    Name = "Jesko Absolut",
                    Brand = "Koenigsegg",
                    Year = "2020",
                    HorsePower = 1600,
                    Picture = "https://www.koenigsegg.com/wp-content/uploads/2020/02/Jesko_absolut_front-1024x612.jpg",
                },
                new Cars
                {
                    Name = "Lamborghini Aventador",
                    Brand = "Lamborghini",
                    Year = "2020",
                    HorsePower = 730,
                    Picture = "https://www.lamborghini.com/sites/it-en/files/DAM/lamborghini/facelift_2019/model_detail/aventador/svj_roadster/overview/01-overview_M.jpg",
                },
                new Cars
                {
                    Name = "Mclaren Speedtail",
                    Brand = "Mclaren",
                    Year = "2020",
                    HorsePower = 1035,
                    Picture = "https://editorial.pxcrush.net/carsales/general/editorial/mclaren-speedtail-ii-u3fq.jpg?width=1024&height=683",
                },
                new Cars
                {
                    Name = "Bugatti Chiron",
                    Brand = "Bugatti",
                    Year = "2020",
                    HorsePower = 1500,
                    Picture = "https://assets.bugatti.com/fileadmin/_processed_/sei/p54/se-image-4799f9106491ebb58ca3351f6df5c44a.jpg",
                },
                new Cars
                {
                    Name = "Audi R8",
                    Brand = "Audi",
                    Year = "2020",
                    HorsePower = 562,
                    Picture = "https://car-images.bauersecure.com/pagefiles/85075/049_r8_v10.jpg",
                },
                new Cars
                {
                    Name = "Toyota GR Super Sport",
                    Brand = "Toyota",
                    Year = "2020",
                    HorsePower = 986,
                    Picture = "https://cdn.motor1.com/images/mgl/j4916/s1/toyota-gr-super-sport-concept.jpg",
                },
                new Cars
                {
                    Name = "Chevrolet corvette ZR1",
                    Brand = "Chevrolet",
                    Year = "2020",
                    HorsePower = 755,
                    Picture = "https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/2020-chevrolet-corvette-lead-1571164761.jpg",
                },
                new Cars
                {
                    Name = "Ford GT",
                    Brand = "Ford",
                    Year = "2020",
                    HorsePower = 647,
                    Picture = "https://s1.cdn.autoevolution.com/images/models/FORD_GT-2019_main.jpg",
                },
                  new Cars
                {
                    Name = "Nissan GT-R",
                    Brand = "Nissan",
                    Year = "2020",
                    HorsePower = 999,
                    Picture = "https://i.kinja-img.com/gawker-media/image/upload/c_fill,f_auto,fl_progressive,g_center,h_675,pg_1,q_80,w_1200/otgnzsjlg4ofh50ovo5n.jpg",
                },
            };

            return Ok(result);
        }


    }



    public class Cars
    {
        public string Name { get; set; }

        public string Brand { get; set; }

        public string Year { get; set; }

        public int? HorsePower { get; set; }

        public string Picture { get; set; }


        public Cars()
        {
            Name = null;
            Brand = null;
            Year = null;
            HorsePower = null;
            Picture = null;
        }


    } 
}